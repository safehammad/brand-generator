===============
Brand Generator
===============

The Brand Generator asks you to enter  one or more key words that represent
the essence of your brand and recursively generates synonyms for these words.
At each stage of synonym generation, the Brand Generator will ask for your
approval or disapproval of each word.

Requirements
------------
This application requires the `Natural Language Toolkit (ntlk) <http://nltk.org/>`_.
Once the nltk is installed, the WordNet database will need to be downloaded.
This is most easily done at the Python prompt::

    >>> import nltk
    >>> nltk.download()

A GUI will appear where you can choose to download WordNet. This only needs
to be done once.
