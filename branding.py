# -*- coding: utf-8 -*-

# Copyright 2013 Safe Hammad.
#
# This file is part of BrandGenerator.
#
# BrandGenerator is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# BrandGenerator is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# BrandGenerator.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import (
    unicode_literals,
    print_function,
    absolute_import,
    division,
)

from itertools import chain, permutations
from collections import namedtuple

from nltk.corpus import wordnet


class WordGroup(object):
    def __init__(self, word):
        self.approved = set([word])
        self.unapproved = set()
        self.unknown = set()
        self.generate_synonyms()

    def update(self):
        """Update the word group with approved and unpproved words."""
        approved, unapproved = approve_words(self.unknown)
        self.approved.update(approved)
        self.unapproved.update(unapproved)
        self.generate_synonyms()

    def generate_synonyms(self):
        self.unknown = synonyms(self.approved) - self.approved - self.unapproved


def input_words():
    """Return key words from the user."""
    print('\nEnter words. Type "end" when finished.\n')
    while True:
        word = raw_input('Next word: ').strip().lower()
        if word == 'end':
            return
        if word:
            yield word


def synonyms(words):
    """Return a set of synonyms for the given words."""
    def wordnym(word):
        return chain.from_iterable(synset.lemma_names for synset in wordnet.synsets(word))
    return set(chain.from_iterable(map(wordnym, words)))


def approve_words(words):
    """Present words to the user to approve or unapprove."""
    print('\nRate the following {} words (y/n):\n'.format(len(words)))
    approved = set()
    unapproved = set()
    for i, word in enumerate(words, start=1):
        choice = raw_input('{:2}. {}? '.format(i, word)).strip().lower()
        if choice == 'y':
            approved.add(word)
        else:
            unapproved.add(word)
    return approved, unapproved


def print_approved_words(word_groups):
    approved_words = set(chain.from_iterable(word_group.approved for word_group in word_groups))
    print('\nResults:\n')
    for word in sorted(approved_words):
        print(word)


def main():
    words = list(input_words())
    word_groups = [WordGroup(word) for word in words]

    # Cycle through each word group approving and removing
    while any(word_group.unknown for word_group in word_groups):
        for word_group in word_groups:
            word_group.update()

    print_approved_words(word_groups)


if __name__ == '__main__':
    main()
